import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'
import 'animate.css/animate.min.css'

Vue.config.productionTip = false

Vue.use(Vuetify, {
  theme: {
    primary: '#2696f3',
  },
})

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
