import axios, { AxiosRequestConfig, AxiosInstance, Canceler, Cancel } from 'axios'
import config from '@/config'

// Idea.
// Cache responses with lifetime. Look for library to do this.
// Store cache in localStorage and share between instances (optional)

// type GitLabCache = {
//   user: any,
//   projects: any[],
//   projectTree: any[],
//   readme: string,
// }

class GitLab {
  private client: AxiosInstance
  // private cache: GitLabCache

  constructor(sharedCache = false, clientConfig?: AxiosRequestConfig) {
    this.client = axios.create({
      baseURL: 'https://gitlab.com/api/v4',
      headers: {
        'Private-Token': config.token,
      },
      ...clientConfig,
    })
  }

  public async searchUsers(needle: string) {
    return this.get('users', { search: needle, per_page: 6 })
  }

  public async getUser(userId: string) {
    return this.get(`users/${userId}`)
  }

  public async getUsers(username: string) {
    return this.get('users', { username })
  }

  public async userProjects(userId: string) {
    return this.get(`users/${userId}/projects`)
  }

  public async searchProjects(needle: string) {
    return this.get('search', { scope: 'projects', search: needle, per_page: 6 })
  }

  public async project(projectId: string) {
    return this.get(`projects/${projectId}`)
  }

  public async projectTree(projectId: string, options?: {}) {
    return this.get(`/projects/${projectId}/repository/tree`, options)
  }

  public async projectFile(projectId: string, fileId: string) {
    const { content, encoding } = await this.get(`/projects/${projectId}/repository/blobs/${fileId}`) as any
    const buffer = new Buffer(content, encoding)
    return buffer.toString('UTF-8')
  }

  private async get(url: string, params?: any) {
    const { data } = await this.client.get(url, { params })
    return data
  }

}

export default GitLab

export { Canceler }
