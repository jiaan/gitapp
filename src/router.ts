import Vue from 'vue'
import Router from 'vue-router'
import Search from './views/Search.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'search',
      component: Search,
    },
    {
      path: '/project/:projectId',
      name: 'project',
      component: () => import(/* webpackChunkName: "project" */ './views/Project.vue'),
      props: true,
    },
    {
      path: '/user/:userName',
      name: 'user',
      component: () => import(/* webpackChunkName: "user" */ './views/User.vue'),
      props: true,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
})
