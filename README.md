# gitapp

This project is a **WIP**!!

This is just a tiny project to fimiliarize myself with vue and the gitlab API.

You can use it to search for a gitlab user and then view their projects.

## Project setup
```
yarn install
```

Create a gitlab access token with read privelidges and add it to a `config.ts` file. 

See `config.example.ts` for a example of the config format.

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
